# Presentation

This program offers various tools to manipulate `.pls` and `.m3u` playlist files.

Part of its code is based on a script by Jan-Matthias Braun available here:

![https://bugs.kde.org/show_bug.cgi?id=333745#c17](https://bugs.kde.org/show_bug.cgi?id=333745#c17)

# How to run

## Docker

You can build and run the docker image using these commands:

    docker build . -t playlist-manager
    docker run -it -v $(pwd):/code -p 8888:8888 playlist-manager bash