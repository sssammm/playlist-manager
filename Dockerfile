FROM python:3.7.11-slim-buster

WORKDIR /code
COPY . /code

RUN pip install -r requirements.txt

CMD ["bash"]
