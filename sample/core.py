# Contains the core functions

from urllib.parse import unquote
import re

def unquote_playlist(filename, suffix="_unqoted"):
    """
    Unquote a playlist file

    Args:
        filename (str): The path to a file to unquote.
        suffix (str): A suffix to add to the unquoted file.
    
    Returns:
        nothing: creates an unquoted copy of the input file.
    """
    # select text after last point and store it as extension
    name = re.findall(r'.*(?=\.\w{1,4}$)', my_file)[0]
    ext = re.findall(r'\.\w{1,4}$', my_file)[-1]
    filename_out = name + suffix + ext 

    f_in = open(filename)
    f_out = open(filename_out, "w")
    
    for line in f_in:
        if line[0] != '#':
            line = unquote(line)
        print(line, end='', file=f_out)


str = 'an example word:cat!!'
match = re.search(r'word:\w\w\w', str)

my_file = "a.weired.file.name.txt"

