# This is an example of how to use playlist-manager

# %% Setup

import playlist-manager as pm

# %% Convert a playlist

pm.convert_playlist(
    path="~/Documents/playlists/funky.pls",
    format_from="pls",
    format_to="m3u",
    suffix="",
    keep_old=True
)

# %% Convert all playlists within a directory

pm.convert_dir(
    path="~/Documents/playlists/",
    format_from="pls",
    format_to="m3u",
    suffix="",
    keep_old=True
)
